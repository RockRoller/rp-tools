import { ensure } from "../base/shared/common"
import { createStringSpan } from "../base/shared/dom"
import { getDiceRoll } from "../base/shared/math"
import { displayResult } from "../shared"

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        const rollButton = ensure(document.querySelector<HTMLButtonElement>("#get-dice"))
        const inputField = ensure(document.querySelector<HTMLInputElement>("#dice-sides"))

        rollButton.addEventListener("click", () => {
            rollDice()
        })
        inputField.addEventListener("keydown", (e) => {
            if(e.key === "Enter"){
                rollDice()
            }
        })
    })
}

function rollDice() {
    const diceSides = parseInt(ensure(document.querySelector<HTMLInputElement>("#dice-sides")).value)
    if(Number.isNaN(diceSides)){
        displayResult([createStringSpan("Please input a valid number!", { className: "bold" })])
        return
    }

    displayResult([createStringSpan("You rolled "), createStringSpan(getDiceRoll(diceSides).toString() + ".", { className: "bold dice-output" })])
}

addButtonListener()
