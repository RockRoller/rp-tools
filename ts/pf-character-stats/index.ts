import { ensure, wait } from "../base/shared/common"
import { createElement } from "../base/shared/dom"
import { getDiceRoll, sumArray } from "../base/shared/math"
import { pathfinderClasses, pathfinderRaces } from "./constants"

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        addFeatureSelectors()
        const rollButton = ensure(document.querySelector<HTMLButtonElement>("#get-stat-dices"))

        rollButton.addEventListener("click", () => {
            rollStatDices()
        })
    })
}

function addFeatureSelectors() {
    addFeatureSelector(".class .selector", "#class-selector", pathfinderClasses)
    addFeatureSelector(".race .selector", "#race-selector", pathfinderRaces)
}

function addFeatureSelector(query: string, id: string, record: Record<string, any>) {
    const target = ensure(document.querySelector(query))
    const selector = createElement("select", {
        attributes: {
            id: id
        }
    })
    Object.keys(record).forEach((key) => {
        const element = createElement("option", {
            attributes: {
                value: key,
                innerText: record[key].name
            }
        })

        selector.append(element)
    })

    target.prepend(selector)
}

async function rollStatDices() {
    indicateLoading("off")
    let stats: Array<number> = []
    for (let i = 0; i < 6; i++) {
        stats.push(await getRolledStat())
        await wait(50)
    }
    displayRolledStats(stats)
}

function displayRolledStats(stats: Array<number>) {
    const targets = document.querySelectorAll<HTMLSpanElement>(".dice .result")

    stats.forEach((v, i) => {
        targets[i].innerText = v.toString()
    })

    indicateLoading("on")
}

async function getRolledStat() {
    const rolls = []
    for (let i = 0; rolls.length < 4; i++) {
        rolls.push(getDiceRoll(6))
        await wait(50)
    }
    return sumArray(removeLowestRoll(rolls))
}

function removeLowestRoll(rolls: Array<number>) {
    rolls = rolls.slice() //copy the array
    rolls.splice(rolls.indexOf(Math.min.apply(null, rolls)), 1)
    return rolls
}

function indicateLoading(state: "on" | "off") {
    const button = ensure(document.querySelector<HTMLButtonElement>("#get-stat-dices"))
    if( state === "off"){
        button.innerText = "Rolling..."
        button.disabled = true
    } else {
        button.innerText = "Roll stats!"
        button.disabled = false
    }
}

addButtonListener()
