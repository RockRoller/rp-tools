export type pathfinderRace = {
    name: string
    stats: pathfinderRaceStats
    note?: string
}

type pathfinderRaceStats = {
    str?: number
    dex?: number
    con?: number
    wis?: number
    int?: number
    cha?: number
    anyPlus?: number
    anyMinus?: number
    skillBonus?: Array<skillBonus>
}

export type pathfinderClass = {
    name: string
    stats: pathfinderClassStats
    skills: Array<skill>
}

type pathfinderClassStats = {
    gab?: number
    for?: number
    ref?: number
    wil?: number
}

type skillBonus = {
    skill: skill
    value: number
}

type skill = "acrobatics" | "appraise" | "bluff" | "climb" | "craft" | "diplomacy" | "disable device" | "disguise" | "escape artist" | "fly" | "handle animal" | "heal" | "intimidate" | "knowledge (arcana)" | "knowledge (dungeoneering)" | "knowledge (engineering)" | "knowledge (geography)" | "knowledge (history)" | "knowledge (local)" | "knowledge (nature)" | "knowledge (nobility)" | "knowledge (nobility)" | "knowledge (planes)" | "knowledge (religion)" | "linguistics" | "perception" | "perform" | "profession" | "ride" | "sense motive" | "sleight of hand" | "spellcraft" | "stealth" | "survivial" | "swim" | "use magic device" | "any" | "knowledge (all)"