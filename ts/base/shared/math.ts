/**
 * This function returns a random integer between 0 and max-1.
 * @param max Maximum integer
 * @returns Integer between 0 and max -1
 */
export function getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max))
}

export function getDiceRoll(sides: number) {
    return getRandomInt(sides) + 1
}

export function sumArray(array: Array<number>) {
    return array.reduce((sum, current) => sum + current)
}
