import * as icons from "../constants/icons"
import { CreateElementOptions, CreateIconOptions, IconType } from "../constants/types"
import { ensure } from "./common"

export function createElementFromHTML<T extends Element>(html: string) {
    const container = document.createElement("div")
    container.innerHTML = html

    return container.firstChild as T
}

/**
 * Convenience function for creating elements with styles and attributes
 */
export function createElement<T extends keyof HTMLElementTagNameMap>(tagName: T, options: CreateElementOptions<T>) {
    const { attributes = {}, style = {}, className, children } = options
    const element = document.createElement(tagName)

    const safeClassName = [`mod-item`, className].filter((x) => x).join(" ")
    Object.assign(element, { ...attributes, className: safeClassName })
    Object.assign(element.style, style)

    if (children) {
        element.append(...children)
    }

    return element
}

const safeAnchorAttributes = {
    rel: "nofollow noreferrer noopener",
    target: "_blank",
}

type CreateAnchorOptions = CreateElementOptions<"a"> & {
    unsafe?: boolean
}

/**
 * Convenience function that creates a safe anchor
 */
export const createAnchor = (href: string, options: CreateAnchorOptions = {}) =>
    createElement("a", {
        ...options,
        attributes: {
            ...(options?.attributes ?? {}),
            ...(options.unsafe ? {} : safeAnchorAttributes),
            href,
        },
    })

/**
 * Convenience function that just creates a span with text in it
 */
export const createStringSpan = (text: string, options: CreateElementOptions<"span"> = {}) =>
    createElement("span", {
        ...options,
        attributes: {
            ...(options?.attributes ?? {}),
            innerText: text,
        },
    })

export const waitForElement = <T extends Element>(query: string, parent: HTMLElement | Document = document) =>
    new Promise<T>((resolve, reject) => {
        let tries = 0

        const find = () => {
            const element = parent.querySelector<T>(query)
            if (element) return resolve(element)

            if (tries > 200) {
                return reject(new Error("waitForElement timed out"))
            }

            tries++
            setTimeout(() => find(), 100)
        }

        find()
    })

export function insertStyleTag(options: string) {
    const head = ensure(document.querySelector("head"))

    const style = createElement("style", {
        attributes: {
            innerHTML: options,
        },
    })

    head.append(style)
}

export function createIcon(name: IconType, options: CreateIconOptions = {}) {
    const { size = 16, ...rest } = options

    const svg = icons[name]
    const element = createElementFromHTML<SVGElement>(svg)

    Object.assign(element.style, {
        width: `${size}px`,
        height: `${size}px`,
        ...rest,
    })

    return element
}