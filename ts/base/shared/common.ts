
export const wait = (ms: number) => new Promise((r) => setTimeout(r, ms))

export function ensure<T>(value: T | null | undefined): T {
    if (!value) {
        throw new Error("Ensured value does not exist")
    }

    return value
}

export async function downloadUrl(url: string, filename: string) {
    const anchor = document.createElement("a")
    document.body.append(anchor)
    const response = await fetch(url)
    const blobUrl = URL.createObjectURL(await response.blob())
    anchor.href = blobUrl
    anchor.download = filename
    anchor.click()
    anchor.remove()
    URL.revokeObjectURL(blobUrl)
}
