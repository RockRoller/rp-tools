import * as icons from "./icons"

export type CreateElementOptions<T extends keyof HTMLElementTagNameMap> = {
    className?: string
    attributes?: Partial<HTMLElementTagNameMap[T]>
    style?: Partial<CSSStyleDeclaration>
    children?: (Element | string)[]
}

export type IconType = keyof typeof icons

export type CreateIconOptions = Partial<CSSStyleDeclaration> & {
    size?: number
}
