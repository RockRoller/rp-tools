import { ensure } from "base/shared/common"
import { createElement } from "base/shared/dom"

export function trimResults() {
    const displayedResults = document.querySelectorAll("#output .result-card")
    const displayedResultsCount = displayedResults.length
    if (displayedResultsCount > 20) {
        displayedResults[displayedResultsCount - 1].remove()
    }
}

export function displayResult(result: Array<HTMLElement>) {
    const target = ensure(document.querySelector("#output tbody"))
    const timestamp = createElement("td", {
        className: "timestamp",
        attributes: {
            innerText: new Date().toLocaleTimeString(),
        },
    })
    const roll = createElement("td", {
        className: "result",
        children: result,
    })
    const row = createElement("tr", {
        className: "result-card",
        children: [timestamp, roll],
    })
    target.prepend(row)
    trimResults()
}