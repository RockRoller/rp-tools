export type attackOptions = {
	damageBase: damageBase;
    damageBaseKey: string;
	stab: boolean;
	criticalHit: boolean;
	ownAttack: number;
	enemyDefense: number;
	typeEffectivity: number;
};

type typeEffectivity = 0.5 | 0.75 | 1 | 1.5 | 2;

export type damageBase = {
	diceType: number;
	diceCount: number;
	flatBonus: number;
};
