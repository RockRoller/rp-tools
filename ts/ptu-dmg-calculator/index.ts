import { ensure } from "../base/shared/common"
import { createStringSpan } from "../base/shared/dom"
import { getDiceRoll } from "../base/shared/math"
import { displayResult } from "../shared"
import { damageBases } from "./constants"
import { attackOptions } from "./type"

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        const rollButton = ensure(document.querySelector<HTMLButtonElement>("#get-damage"))

        rollButton.addEventListener("click", () => {
            rollDamage()
        })
    })
}

/**
 * This function calculates the damage of an attack based on inputted options.
 * @param attackOptions Options for an attack, @see attackOptions
 * @returns damage (int)
 */
function calculateDamage(attackOptions: attackOptions) {
    // check ownAttack and enemyDefense
    if (Number.isNaN(attackOptions.ownAttack)) {
        attackOptions.ownAttack = 0
    }
    if (Number.isNaN(attackOptions.enemyDefense)) {
        attackOptions.enemyDefense = 0
    }
    // find initial dmgBase + apply STAB
    if (attackOptions.stab == true) {
        attackOptions.damageBase = upgradeDamageBase(attackOptions.damageBaseKey, 2)
    }
    // roll damage (while respecting crit)
    let diceDamage = 0
    for (let i = 0; i < attackOptions.damageBase.diceCount * (attackOptions.criticalHit === true ? 2 : 1); i++) {
        diceDamage += getDiceRoll(attackOptions.damageBase.diceType)
    }
    // add flatBonu (while respecting crit), add attack/defense, apply type effectivity
    return Math.floor((diceDamage + attackOptions.damageBase.flatBonus * (attackOptions.criticalHit === true ? 2 : 1) + attackOptions.ownAttack - attackOptions.enemyDefense) * attackOptions.typeEffectivity)
}

function upgradeDamageBase(currentDamageBaseKey: string, levels: number) {
    return damageBases[`dmgBase${parseInt(currentDamageBaseKey.replace("dmgBase", "")) + levels}`]
}

function rollDamage() {
    displayResult([createStringSpan("You rolled "), createStringSpan(calculateDamage(ensure(getAttackOptions())).toString(), { className: "bold damage-output" }), createStringSpan(" damage points.")])
}

function getAttackOptions() {
    try {
        const attackOptions: attackOptions = {
            damageBase: damageBases[ensure(document.querySelector<HTMLSelectElement>("#damage-base")).value],
            damageBaseKey: ensure(document.querySelector<HTMLSelectElement>("#damage-base")).value,
            typeEffectivity: parseFloat(ensure(document.querySelector<HTMLSelectElement>("#type-effectivity")).value),
            ownAttack: parseInt(eval(ensure(document.querySelector<HTMLInputElement>("#own-attack")).value)),
            enemyDefense: parseInt(eval(ensure(document.querySelector<HTMLInputElement>("#enemy-defense")).value)),
            stab: ensure(document.querySelector<HTMLInputElement>("#stab")).checked,
            criticalHit: ensure(document.querySelector<HTMLInputElement>("#critical-hit")).checked,
        }
        return attackOptions
    } catch (e) {
        displayResult([createStringSpan("Please input valid numbers!", { className: "bold" })])
    }
}

addButtonListener()
