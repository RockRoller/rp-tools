import ts from "@wessberg/rollup-plugin-ts";

export default {
    input: 'src/index.ts',
    output: {
        file: 'script.user.js',
        format: 'cjs'
    },
    plugins: [
        ts({
        })
    ]
};