'use strict';

function ensure(value) {
    if (!value) {
        throw new Error("Ensured value does not exist");
    }
    return value;
}

/**
 * Convenience function for creating elements with styles and attributes
 */
function createElement(tagName, options) {
    const { attributes = {}, style = {}, className, children } = options;
    const element = document.createElement(tagName);
    const safeClassName = [`mod-item`, className].filter((x) => x).join(" ");
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: safeClassName }));
    Object.assign(element.style, style);
    if (children) {
        element.append(...children);
    }
    return element;
}
/**
 * Convenience function that just creates a span with text in it
 */
const createStringSpan = (text, options = {}) => {
    var _a;
    return createElement("span", Object.assign(Object.assign({}, options), { attributes: Object.assign(Object.assign({}, ((_a = options === null || options === void 0 ? void 0 : options.attributes) !== null && _a !== void 0 ? _a : {})), { innerText: text }) }));
};

/**
 * This function returns a random integer between 0 and max-1.
 * @param max Maximum integer
 * @returns Integer between 0 and max -1
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function getDiceRoll(sides) {
    return getRandomInt(sides) + 1;
}

function trimResults() {
    const displayedResults = document.querySelectorAll("#output .result-card");
    const displayedResultsCount = displayedResults.length;
    if (displayedResultsCount > 20) {
        displayedResults[displayedResultsCount - 1].remove();
    }
}
function displayResult(result) {
    const target = ensure(document.querySelector("#output tbody"));
    const timestamp = createElement("td", {
        className: "timestamp",
        attributes: {
            innerText: new Date().toLocaleTimeString(),
        },
    });
    const roll = createElement("td", {
        className: "result",
        children: result,
    });
    const row = createElement("tr", {
        className: "result-card",
        children: [timestamp, roll],
    });
    target.prepend(row);
    trimResults();
}

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        const rollButton = ensure(document.querySelector("#get-dice"));
        const inputField = ensure(document.querySelector("#dice-sides"));
        rollButton.addEventListener("click", () => {
            rollDice();
        });
        inputField.addEventListener("keydown", (e) => {
            if (e.key === "Enter") {
                rollDice();
            }
        });
    });
}
function rollDice() {
    const diceSides = parseInt(ensure(document.querySelector("#dice-sides")).value);
    if (Number.isNaN(diceSides)) {
        displayResult([createStringSpan("Please input valid numbers!", { className: "bold" })]);
        return;
    }
    displayResult([createStringSpan("You rolled "), createStringSpan(getDiceRoll(diceSides).toString() + ".", { className: "bold dice-output" })]);
}
addButtonListener();
