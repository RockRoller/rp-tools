'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const wait = (ms) => new Promise((r) => setTimeout(r, ms));
function ensure(value) {
    if (!value) {
        throw new Error("Ensured value does not exist");
    }
    return value;
}

/**
 * Convenience function for creating elements with styles and attributes
 */
function createElement(tagName, options) {
    const { attributes = {}, style = {}, className, children } = options;
    const element = document.createElement(tagName);
    const safeClassName = [`mod-item`, className].filter((x) => x).join(" ");
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: safeClassName }));
    Object.assign(element.style, style);
    if (children) {
        element.append(...children);
    }
    return element;
}

/**
 * This function returns a random integer between 0 and max-1.
 * @param max Maximum integer
 * @returns Integer between 0 and max -1
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function getDiceRoll(sides) {
    return getRandomInt(sides) + 1;
}
function sumArray(array) {
    return array.reduce((sum, current) => sum + current);
}

const pathfinderRaces = {
    elf: { name: "elf", stats: { dex: 2, int: 2, con: -2, skillBonus: [{ skill: "perception", value: 2 }] } },
    gnome: {
        name: "gnome",
        stats: {
            con: 2,
            cha: 2,
            str: -2,
            skillBonus: [
                { skill: "profession", value: 2 },
                { skill: "craft", value: 2 },
            ],
        },
        note: "Only craft OR profession",
    },
    halfElf: { name: "half elf", stats: { anyPlus: 2, skillBonus: [{ skill: "perception", value: 2 }] } },
    halfOrk: { name: "half ork", stats: { anyPlus: 2, skillBonus: [{ skill: "intimidate", value: 2 }] } },
    halfling: {
        name: "halfling",
        stats: {
            dex: 2,
            cha: 2,
            str: -2,
            skillBonus: [
                { skill: "perception", value: 2 },
                { skill: "acrobatics", value: 2 },
                { skill: "climb", value: 2 },
            ],
        },
    },
    human: { name: "human", stats: { anyPlus: 2 } },
};
const pathfinderClasses = {
    barbarian: {
        name: "barbarian",
        stats: { gab: 1, for: 2 },
        skills: ["acrobatics", "intimidate", "craft", "climb", "handle animal", "ride", "swim", "survivial", "perception", "knowledge (nature)"],
    },
    bard: {
        name: "bard",
        stats: { wil: 2, ref: 2 },
        skills: [
            "acrobatics",
            "perform",
            "profession",
            "bluff",
            "diplomacy",
            "intimidate",
            "escape artist",
            "sleight of hand",
            "craft",
            "stealth",
            "climb",
            "use magic device",
            "sense motive",
            "appraise",
            "linguistics",
            "disguise",
            "perception",
            "knowledge (all)",
            "spellcraft",
        ],
    },
    druid: {
        name: "druid",
        stats: { wil: 2, for: 2 },
        skills: ["profession", "fly", "craft", "heal", "climb", "handle animal", "ride", "swim", "perception", "knowledge (geography)", "knowledge (nature)", "survivial", "spellcraft"],
    },
    sorcerer: {
        name: "sorcerer",
        stats: { wil: 2 },
        skills: ["profession", "bluff", "intimidate", "fly", "craft", "use magic device", "appraise", "knowledge (arcana)", "spellcraft"],
    },
    fighter: {
        name: "fighter",
        stats: { gab: 1, for: 2 },
        skills: ["profession", "intimidate", "craft", "climb", "handle animal", "ride", "swim", "survivial", "knowledge (engineering)", "knowledge (dungeoneering)"],
    },
    cleric: {
        name: "cleric",
        stats: { wil: 2, for: 2 },
        skills: [
            "profession",
            "diplomacy",
            "craft",
            "heal",
            "sense motive",
            "appraise",
            "linguistics",
            "knowledge (nobility)",
            "knowledge (arcana)",
            "knowledge (history)",
            "knowledge (planes)",
            "knowledge (religion)",
            "spellcraft",
        ],
    },
    wizard: {
        name: "wizard",
        stats: { wil: 2 },
        skills: ["profession", "fly", "craft", "appraise", "linguistics", "knowledge (all)", "spellcraft"],
    },
    monk: {
        name: "monk",
        stats: { ref: 2, wil: 2, for: 2 },
        skills: [
            "acrobatics",
            "perform",
            "profession",
            "intimidate",
            "escape artist",
            "craft",
            "stealth",
            "climb",
            "sense motive",
            "ride",
            "swim",
            "perception",
            "knowledge (history)",
            "knowledge (religion)",
        ],
    },
    paladin: {
        name: "paladin",
        stats: { gab: 1, wil: 2, for: 2 },
        skills: ["profession", "diplomacy", "craft", "heal", "handle animal", "sense motive", "ride", "knowledge (nobility)", "knowledge (religion)", "spellcraft"],
    },
    rogue: {
        name: "rogue",
        stats: { ref: 2 },
        skills: [
            "acrobatics",
            "perform",
            "profession",
            "bluff",
            "diplomacy",
            "intimidate",
            "escape artist",
            "sleight of hand",
            "craft",
            "stealth",
            "climb",
            "use magic device",
            "disable device",
            "sense motive",
            "appraise",
            "swim",
            "linguistics",
            "disguise",
            "perception",
            "knowledge (dungeoneering)",
            "knowledge (local)",
        ],
    },
    ranger: {
        name: "ranger",
        stats: { gab: 1, ref: 2, for: 2 },
        skills: [
            "profession",
            "intimidate",
            "craft",
            "heal",
            "stealth",
            "climb",
            "handle animal",
            "ride",
            "swim",
            "survivial",
            "perception",
            "knowledge (geography)",
            "knowledge (dungeoneering)",
            "knowledge (nature)",
            "spellcraft",
        ],
    },
};

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        addFeatureSelectors();
        const rollButton = ensure(document.querySelector("#get-stat-dices"));
        rollButton.addEventListener("click", () => {
            rollStatDices();
        });
    });
}
function addFeatureSelectors() {
    addFeatureSelector(".class .selector", "#class-selector", pathfinderClasses);
    addFeatureSelector(".race .selector", "#race-selector", pathfinderRaces);
}
function addFeatureSelector(query, id, record) {
    const target = ensure(document.querySelector(query));
    const selector = createElement("select", {
        attributes: {
            id: id
        }
    });
    Object.keys(record).forEach((key) => {
        const element = createElement("option", {
            attributes: {
                value: key,
                innerText: record[key].name
            }
        });
        selector.append(element);
    });
    target.prepend(selector);
}
function rollStatDices() {
    return __awaiter(this, void 0, void 0, function* () {
        indicateLoading("off");
        let stats = [];
        for (let i = 0; i < 6; i++) {
            stats.push(yield getRolledStat());
            yield wait(50);
        }
        displayRolledStats(stats);
    });
}
function displayRolledStats(stats) {
    const targets = document.querySelectorAll(".dice .result");
    stats.forEach((v, i) => {
        targets[i].innerText = v.toString();
    });
    indicateLoading("on");
}
function getRolledStat() {
    return __awaiter(this, void 0, void 0, function* () {
        const rolls = [];
        for (let i = 0; rolls.length < 4; i++) {
            rolls.push(getDiceRoll(6));
            yield wait(50);
        }
        return sumArray(removeLowestRoll(rolls));
    });
}
function removeLowestRoll(rolls) {
    rolls = rolls.slice(); //copy the array
    rolls.splice(rolls.indexOf(Math.min.apply(null, rolls)), 1);
    return rolls;
}
function indicateLoading(state) {
    const button = ensure(document.querySelector("#get-stat-dices"));
    if (state === "off") {
        button.innerText = "Rolling...";
        button.disabled = true;
    }
    else {
        button.innerText = "Roll stats!";
        button.disabled = false;
    }
}
addButtonListener();
