'use strict';

function ensure(value) {
    if (!value) {
        throw new Error("Ensured value does not exist");
    }
    return value;
}

/**
 * Convenience function for creating elements with styles and attributes
 */
function createElement(tagName, options) {
    const { attributes = {}, style = {}, className, children } = options;
    const element = document.createElement(tagName);
    const safeClassName = [`mod-item`, className].filter((x) => x).join(" ");
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: safeClassName }));
    Object.assign(element.style, style);
    if (children) {
        element.append(...children);
    }
    return element;
}
/**
 * Convenience function that just creates a span with text in it
 */
const createStringSpan = (text, options = {}) => {
    var _a;
    return createElement("span", Object.assign(Object.assign({}, options), { attributes: Object.assign(Object.assign({}, ((_a = options === null || options === void 0 ? void 0 : options.attributes) !== null && _a !== void 0 ? _a : {})), { innerText: text }) }));
};

/**
 * This function returns a random integer between 0 and max-1.
 * @param max Maximum integer
 * @returns Integer between 0 and max -1
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function getDiceRoll(sides) {
    return getRandomInt(sides) + 1;
}

function trimResults() {
    const displayedResults = document.querySelectorAll("#output .result-card");
    const displayedResultsCount = displayedResults.length;
    if (displayedResultsCount > 20) {
        displayedResults[displayedResultsCount - 1].remove();
    }
}
function displayResult(result) {
    const target = ensure(document.querySelector("#output tbody"));
    const timestamp = createElement("td", {
        className: "timestamp",
        attributes: {
            innerText: new Date().toLocaleTimeString(),
        },
    });
    const roll = createElement("td", {
        className: "result",
        children: result,
    });
    const row = createElement("tr", {
        className: "result-card",
        children: [timestamp, roll],
    });
    target.prepend(row);
    trimResults();
}

const damageBases = {
    dmgBase1: { diceCount: 1, diceType: 6, flatBonus: 1 },
    dmgBase2: { diceCount: 1, diceType: 6, flatBonus: 3 },
    dmgBase3: { diceCount: 1, diceType: 6, flatBonus: 5 },
    dmgBase4: { diceCount: 1, diceType: 8, flatBonus: 6 },
    dmgBase5: { diceCount: 1, diceType: 8, flatBonus: 8 },
    dmgBase6: { diceCount: 2, diceType: 6, flatBonus: 8 },
    dmgBase7: { diceCount: 2, diceType: 6, flatBonus: 10 },
    dmgBase8: { diceCount: 2, diceType: 8, flatBonus: 10 },
    dmgBase9: { diceCount: 2, diceType: 10, flatBonus: 10 },
    dmgBase10: { diceCount: 3, diceType: 8, flatBonus: 10 },
    dmgBase11: { diceCount: 3, diceType: 10, flatBonus: 10 },
    dmgBase12: { diceCount: 3, diceType: 12, flatBonus: 10 },
    dmgBase13: { diceCount: 4, diceType: 10, flatBonus: 10 },
    dmgBase14: { diceCount: 4, diceType: 10, flatBonus: 15 },
    dmgBase15: { diceCount: 4, diceType: 10, flatBonus: 20 },
    dmgBase16: { diceCount: 5, diceType: 10, flatBonus: 20 },
    dmgBase17: { diceCount: 5, diceType: 12, flatBonus: 25 },
    dmgBase18: { diceCount: 6, diceType: 12, flatBonus: 25 },
    dmgBase19: { diceCount: 6, diceType: 12, flatBonus: 30 },
    dmgBase20: { diceCount: 6, diceType: 12, flatBonus: 35 },
    dmgBase21: { diceCount: 6, diceType: 12, flatBonus: 40 },
    dmgBase22: { diceCount: 6, diceType: 12, flatBonus: 45 },
    dmgBase23: { diceCount: 6, diceType: 12, flatBonus: 50 },
    dmgBase24: { diceCount: 6, diceType: 12, flatBonus: 55 },
    dmgBase25: { diceCount: 6, diceType: 12, flatBonus: 60 },
    dmgBase26: { diceCount: 7, diceType: 12, flatBonus: 65 },
    dmgBase27: { diceCount: 8, diceType: 12, flatBonus: 70 },
    dmgBase28: { diceCount: 8, diceType: 12, flatBonus: 80 }
};

/**
 * This function adds all event listener to elements.
 */
function addButtonListener() {
    window.addEventListener("load", () => {
        const rollButton = ensure(document.querySelector("#get-damage"));
        rollButton.addEventListener("click", () => {
            rollDamage();
        });
    });
}
/**
 * This function calculates the damage of an attack based on inputted options.
 * @param attackOptions Options for an attack, @see attackOptions
 * @returns damage (int)
 */
function calculateDamage(attackOptions) {
    // check ownAttack and enemyDefense
    if (Number.isNaN(attackOptions.ownAttack)) {
        attackOptions.ownAttack = 0;
    }
    if (Number.isNaN(attackOptions.enemyDefense)) {
        attackOptions.enemyDefense = 0;
    }
    // find initial dmgBase + apply STAB
    if (attackOptions.stab == true) {
        attackOptions.damageBase = upgradeDamageBase(attackOptions.damageBaseKey, 2);
    }
    // roll damage (while respecting crit)
    let diceDamage = 0;
    for (let i = 0; i < attackOptions.damageBase.diceCount * (attackOptions.criticalHit === true ? 2 : 1); i++) {
        diceDamage += getDiceRoll(attackOptions.damageBase.diceType);
    }
    // add flatBonu (while respecting crit), add attack/defense, apply type effectivity
    return Math.floor((diceDamage + attackOptions.damageBase.flatBonus * (attackOptions.criticalHit === true ? 2 : 1) + attackOptions.ownAttack - attackOptions.enemyDefense) * attackOptions.typeEffectivity);
}
function upgradeDamageBase(currentDamageBaseKey, levels) {
    return damageBases[`dmgBase${parseInt(currentDamageBaseKey.replace("dmgBase", "")) + levels}`];
}
function rollDamage() {
    displayResult([createStringSpan("You rolled "), createStringSpan(calculateDamage(ensure(getAttackOptions())).toString(), { className: "bold damage-output" }), createStringSpan(" damage points.")]);
}
function getAttackOptions() {
    try {
        const attackOptions = {
            damageBase: damageBases[ensure(document.querySelector("#damage-base")).value],
            damageBaseKey: ensure(document.querySelector("#damage-base")).value,
            typeEffectivity: parseFloat(ensure(document.querySelector("#type-effectivity")).value),
            ownAttack: parseInt(eval(ensure(document.querySelector("#own-attack")).value)),
            enemyDefense: parseInt(eval(ensure(document.querySelector("#enemy-defense")).value)),
            stab: ensure(document.querySelector("#stab")).checked,
            criticalHit: ensure(document.querySelector("#critical-hit")).checked,
        };
        return attackOptions;
    }
    catch (e) {
        displayResult([createStringSpan("Please input valid numbers!", { className: "bold" })]);
    }
}
addButtonListener();
